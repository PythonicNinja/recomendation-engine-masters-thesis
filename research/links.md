## Literature > 10 pages

1. Polish 2nd place in 2009 netflix challange
[arek-paterek.com](http://arek-paterek.com/)

2. Stanford recommendation engine summary
[stanford](http://infolab.stanford.edu/~ullman/mmds/ch9.pdf)

3. Bachelor Thesis on topic
[pw bachelor thesis](https://www.facebook.com/l.php?u=https%3A%2F%2Frepo.pw.edu.pl%2Fdocstore%2Fdownload.seam%253Bjsessionid%3D894A76572F0E15E42F2B177E1098D2B6%3FfileId%3DWUT307327&h=0AQFYJIOp)

## Literature < 10 pages

1. Slides How to Build a Recommendation Engine on Spark
[presentation](http://www.slideshare.net/CasertaConcepts/analytics-week-recommendations-on-spark)

## Sites connected with topic

1. Polish startup connected with film reccomendataion engine
[filmmaster.tv](http://filmaster.tv/)


## Technologies

1. Spark 
[Lightning-fast cluster computing](https://spark.apache.org/)

2. Hadoop
[open-source software for reliable, scalable, distributed computing](http://hadoop.apache.org/)